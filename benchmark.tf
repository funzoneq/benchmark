# Configure the OpenStack Provider
provider "openstack" {
    user_name         = "xxxxx@xxxxx.xxx"
    tenant_name       = "BVN000004 stress"
    password          = "xxxxx"
    auth_url          = "https://identity.openstack.cloudvps.com/v3"
    region            = "AMS"
}

# Dashboard
resource "openstack_compute_instance_v2" "dashboard-001" {
    name      = "dashboard-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Standard 4GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "dashboard"
    }
}

# Directory
resource "openstack_compute_instance_v2" "directory-001" {
    name      = "directory-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 2GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "directory"
    }
}

# Benchmark API
resource "openstack_compute_instance_v2" "benchmark-api-001" {
    name      = "benchmark-api-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 2GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "benchmark_api"
    }
}
resource "openstack_compute_instance_v2" "benchmark-api-002" {
    name      = "benchmark-api-002"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 2GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "benchmark_api"
    }
}

# Inways
resource "openstack_compute_instance_v2" "inway-001" {
    name      = "inway-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 8GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "inway"
    }
}
resource "openstack_compute_instance_v2" "inway-002" {
    name      = "inway-002"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 8GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "inway"
    }
}

# TxLog
resource "openstack_compute_instance_v2" "transaction-log-in-001" {
    name      = "transaction-log-in-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Standard 32GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "transaction_log"
    }
}
resource "openstack_compute_instance_v2" "transaction-log-out-001" {
    name      = "transaction-log-out-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Standard 32GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "transaction_log"
    }
}

# Create outways
resource "openstack_compute_instance_v2" "outway-001" {
    name      = "outway-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 4GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "outway"
    }
}
resource "openstack_compute_instance_v2" "outway-002" {
    name      = "outway-002"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 4GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "outway"
    }
}

# Benchmark clients
resource "openstack_compute_instance_v2" "benchmark-client-001" {
    name      = "benchmark-client-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 2GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "benchmark_client"
    }
}
resource "openstack_compute_instance_v2" "benchmark-client-002" {
    name      = "benchmark-client-002"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 2GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "benchmark_client"
    }
}
