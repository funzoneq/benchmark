#!/usr/bin/env bash

PATH="${GOPATH:-${HOME}/go}/bin:${PATH}"

nlx-inway \
  --directory-registration-address "directory-registration-api:8092" \
  --listen-address "127.0.0.1:8443" \
  --self-address "inway:8443" \
  --service-config "service-config.toml" \
  --tls-nlx-root-cert "tls/root.crt" \
  --tls-org-cert "tls/inway.crt" \
  --tls-org-key "tls/inway.key" \
  --disable-logdb \
  --log-type "local" \
  --log-level "warn" \
